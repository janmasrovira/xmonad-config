#!/usr/bin/env fish

set tempfile (mktemp -p /tmp color-XXXX.avif)
set colorList "$(convert -list color | awk 'NF>3{print $1}')"
set color (command echo -e $colorList | rofi -dmenu -i)
convert -size 2x2 xc:$color $tempfile
feh --bg-fill $tempfile
