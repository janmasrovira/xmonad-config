module Base
  ( module Base,
    module Control.Monad,
    module Prelude,
    module Lens.Micro,
    module Lens.Micro.TH,
    module Data.List,
    module XMonad.Util.Run,
    module Data.Singletons,
    module Data.Singletons.Sigma,
    module Data.Singletons.TH,
  )
where

import Control.Monad
import Data.Char
import Data.Kind qualified as GHC
import Data.List (delete, intercalate, sort)
import Data.Map qualified as Map
import Data.Singletons
import Data.Singletons.Sigma
import Data.Singletons.TH (genSingletons)
import GHC.IO.Handle
import Lens.Micro
import Lens.Micro.TH
import Numeric.Natural
import System.Directory
import System.Exit
import System.FilePath
import System.Process
import XMonad
import XMonad.StackSet (current, screen)
import XMonad.Util.Run hiding (proc)
import Prelude

type GHCType = GHC.Type

impossible :: a
impossible = error "impossible"

modm :: KeyMask
modm = mod4Mask

data Selection item
  = None
  | Item item
  | Error String

runPipeNoInput :: (MonadIO m) => [(FilePath, [String])] -> m String
runPipeNoInput pipe = runPipe pipe ""

runPipe :: (MonadIO m) => [(FilePath, [String])] -> String -> m String
runPipe pipe input = case pipe of
  [] -> error "empty pipe"
  [(p, args)] -> runProcessWithInput p args input
  (p, args) : ps -> runProcessWithInput p args input >>= runPipe ps

runProcessNoInput :: (MonadIO m) => FilePath -> [String] -> m String
runProcessNoInput prog args = runProcessWithInput prog args ""

spawnProcess :: (MonadIO m) => FilePath -> [String] -> m ()
spawnProcess prog args = void (runProcessNoInput prog args)

getCurrentScreen :: X ScreenId
getCurrentScreen = gets (screen . current . windowset)

say' :: (MonadIO m) => Natural -> String -> m ()
say' secs msg = spawn $ "echo " ++ msg ++ " | dzen2 -p " <> show secs <> " -xs 1"

say :: (MonadIO m) => String -> m ()
say = say' 2

sayLong :: String -> X ()
sayLong = say' 8

alert :: (MonadIO m) => String -> m ()
alert msg = void $ runProcessNoInput "rofi" ["-e", msg]

askList :: (MonadIO m) => [item] -> (item -> String) -> m (Selection item)
askList items toStr = do
  titl <- delete '\n' <$> runProcessWithInput "rofi" ["-dmenu", "-i"] itemsStr
  return $
    case (titl, findItem titl) of
      ("", _) -> None
      (_, Nothing) -> Error (msg titl)
      (_, Just g) -> Item g
  where
    itemsStr = intercalate "\n" (map toStr items)
    msg txt = "error finding item '" ++ txt ++ "'"
    findItem txt = Map.lookup txt mp
    mp = Map.fromList [(toStr i, i) | i <- items]

askListDo :: [item] -> (item -> String) -> (item -> X ()) -> X ()
askListDo lst f doSomething = do
  sel <- askList lst f
  case sel of
    None -> return ()
    Error msg -> say msg
    Item a -> doSomething a

askListDoSimple :: [(String, X ())] -> X ()
askListDoSimple lst = askListDo lst fst snd

checkPidAliveold :: (MonadIO m) => Pid -> m Bool
checkPidAliveold pid = do
  let p = proc "kill" ["-0", "--timeout 300", show pid]
  (exitCode, _, _) <- liftIO $ readCreateProcessWithExitCode p ""
  return $ case exitCode of
    ExitSuccess -> True
    ExitFailure {} -> False

checkPidAlive1 :: (MonadIO m) => Pid -> m Bool
checkPidAlive1 pid = do
  (_, err) <- runProcessWithInputE "kill" ["-0", show pid] ""
  return $ case err of
    "" -> True
    _ -> False

checkPidAlive :: (MonadIO m) => Pid -> m Bool
checkPidAlive pid =
  liftIO $ doesDirectoryExist ("/proc" </> show pid)

getSteamPid :: (MonadIO m) => m Pid
getSteamPid = liftIO $ do
  home <- getHomeDirectory
  let path = home </> ".steampid"
  read <$> readFile path

checkSteamAlive :: (MonadIO m) => m Bool
checkSteamAlive = getSteamPid >>= checkPidAlive

-- | Code adapted from runProcessWithInput in XMonad.Util.Run to also return
-- sterr. Note that 'readCreateProcessWithExitCode' does not work (pending
-- investigation) in xmonad.
runProcessWithInputE :: (MonadIO m) => FilePath -> [String] -> String -> m (String, String)
runProcessWithInputE cmd args input = io $ do
  (pin, pout, perr, _) <-
    runInteractiveProcess cmd args Nothing Nothing
  hPutStr pin input
  hClose pin
  output <- hGetContents pout
  err <- hGetContents perr
  when (output == output) $ return ()
  when (err == err) $ return ()
  hClose pout
  hClose perr
  -- no need to waitForProcess, we ignore SIGCHLD
  return (output, err)

-- | Remove leading and trailing whitespace.
strip :: String -> String
strip = lstrip . rstrip

-- | Remove leading whitespace.
lstrip :: String -> String
lstrip = dropWhile isSpace

-- | Remove trailing whitespace.
rstrip :: String -> String
rstrip = reverse . lstrip . reverse
