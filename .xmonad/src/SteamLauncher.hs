module SteamLauncher (gameMenu) where

import Base
import Data.Tuple.Extra
import System.Directory
import XMonad

type GameId = Int

type Title = String

type Game m = (Title, GameRun m)

data GameRun m
  = Simple GameId
  | Custom (m ())

games :: [Game m]
games =
  map
    (second Simple)
    [ ("Shogun Showdown ⚔️", 2084000),
      ("Monster Train 🚂", 1102190),
      ("SpellRogue 🎲", 1990110),
      ("Balatro🃏", 2379780),
      ("Nova Drift 🚀", 858210),
      ("Lonestar☄️", 2056210),
      ("Baldur's Gate 3🐙", 1086940),
      ("Book of Demons", 449960),
      ("Thronefall", 2239150),
      ("Ori and the Will of the Wisps 🦉", 1057090),
      ("Deep Rock Galactic ⛏️", 548430),
      ("Against the Storm ⛈️", 1336490),
      ("Vampire Survivors 🧛", 1794680),
      ("Enter the Gungeon 🔫", 311690),
      ("Hades 😈", 1145360),
      ("Persona 5 Royal", 1687950),
      ("Circadian Dice 🎲", 1893620),
      ("Brotato 🥔", 1942280),
      ("Geometry Arena", 1255650),
      ("Into the Breach 🤖", 590380),
      ("Creeper World 4", 848480),
      ("Ruined King: A League of Legends Story", 1276790),
      ("Spirits of the Hellements", 1742610),
      ("Battle Chasers: Nightwar", 451020),
      ("Wildermyth", 763890),
      ("Dragon Quest XI", 1295510),
      ("Crusader Kings 3", 1158310),
      ("Arcanium", 1056840),
      ("Gladiator Guild Manager", 1043260),
      ("Totally Accurate Battle Simulator", 508440),
      ("As Far as the Eye", 1119700),
      ("Northgard", 466560),
      ("Luck be a Landlord", 1404850),
      ("Cookie Clicker", 1454400),
      ("Plague Inc: Evolved", 246620),
      ("Grifltands", 601840),
      ("Mindustry", 1127400),
      ("Among Us", 945360),
      ("Kingdom Rush Vengeance", 1367550),
      ("Faster than Light", 212680),
      ("Streets of Rogue", 512900),
      ("Divinity: Original Sin 2", 435150),
      ("Vault of the Void", 1135810),
      ("Age of Empires III: Definitive Edition", 933110),
      ("Dead Cells", 588650),
      ("Parkitect", 453090),
      ("Rimworld", 294100),
      ("Kerbal Space Program", 220200),
      ("Shadowrun: Dragonfall", 300550),
      ("Besiege", 346010),
      ("Pillars of Eternity II: Deadfire", 560130),
      ("Spelunky", 239350),
      ("Don't Starve Together", 322330),
      ("Warhammer 40k: Mechanicus", 673880),
      ("Battleblock Theater", 238460),
      ("Total War: Warhammer 2", 594570),
      ("Risk of Rain 2", 632360),
      ("Trials of Fire", 1038370),
      ("Bloons TD 6", 960090),
      ("GemCraft Frostborn Wrath", 1106530),
      ("Dungeons 3", 493900),
      ("Cities: Skylines", 255710),
      ("Grim Dawn", 219990),
      ("Renowned Explorers: International Society", 296970),
      ("Thea 2: The Shattering", 606230)
    ]

customGames :: [Game X]
customGames =
  [ ( "Slay the Spire (Mods) 🌱",
      Custom $ do
        liftIO $ setCurrentDirectory stsPath
        steamRunning <- checkSteamAlive
        if steamRunning
          then spawn runStS
          else do
            spawn "steam"
            spawn ("sleep 6 && " ++ runStS)
    )
  ]
  where
    stsPath :: FilePath
    stsPath = "/home/jan/.local/share/Steam/steamapps/common/SlayTheSpire/"
    runStS :: String
    runStS = "DRI_PRIME=1 java -jar mts-launcher.jar"

allGames :: [Game X]
allGames =
  customGames
    ++ games

selectGame :: X (Selection (Game X))
selectGame = askList allGames fst

runGame :: GameRun X -> X ()
runGame g = case g of
  Simple gid -> spawn ("steam steam://rungameid/" ++ show gid)
  Custom c -> c

gameMenu :: X ()
gameMenu = void $ do
  sel <- selectGame
  case sel of
    Error msg -> alert msg
    None -> return ()
    Item (_, spec) -> runGame spec
