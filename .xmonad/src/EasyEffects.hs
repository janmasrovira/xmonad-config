module EasyEffects where

import Base
import Headphones
import XMonad

data Preset = Preset
  { _presetHeadphone :: Headphone,
    _presetMedal :: Medal,
    _presetMeasurement :: Measurement
  }
  deriving stock (Show, Eq, Ord)

data Medal
  = Gold
  | Silver
  | Copper
  | Poo
  | NoMedal
  deriving stock (Show, Eq, Ord)

data Measurement
  = Oracle1990
  | Oratory1990
  | Crinacle
  | Freeryder05
  | Rtings
  deriving stock (Eq, Ord)

instance Show Measurement where
  show :: Measurement -> String
  show = \case
    Oracle1990 -> "oracle1990" -- TODO rename to oratory1990
    Oratory1990 -> "oratory1990"
    Crinacle -> "crinacle"
    Freeryder05 -> "freeryder05"
    Rtings -> "rtings"

ppMedal :: Medal -> String
ppMedal = \case
  Gold -> "🥇"
  Silver -> "🥈"
  Copper -> "🥉"
  Poo -> "💩"
  NoMedal -> ""

presetCliStr :: Preset -> String
presetCliStr Preset {..} = show _presetHeadphone <> " - " <> show _presetMeasurement

ppPreset :: Preset -> String
ppPreset p@Preset {..} = presetCliStr p <> " " <> ppMedal _presetMedal

easyEffectMenu :: X ()
easyEffectMenu =
  askListDoSimple
    [ ("🎧 Select preset", easyEffectPresetMenu),
      ("✅ Enable", spawn "easyeffects -b 2"),
      ("❌ Disable", spawn "easyeffects -b 1")
    ]

mkPreset :: forall (b :: HeadphoneBrand). SHeadphoneBrand b -> HeadphoneModelType b -> Preset
mkPreset = undefined

presets :: [Preset]
presets =
  sort
    [ Preset (mkHeadphone SHifiman Arya) Gold Oracle1990,
      Preset (mkHeadphone SHifiman HE6seV2) Gold Crinacle,
      Preset (mkHeadphone SHifiman HE6seV2) Gold Oratory1990,
      Preset (mkHeadphone SFocal ClearMG) Gold Rtings,
      Preset (mkHeadphone SFocal ClearMG) NoMedal Oracle1990,
      Preset (mkHeadphone SFocal ClearMG) NoMedal Crinacle,
      Preset (mkHeadphone SThieAudio Monarch) Gold Crinacle,
      Preset (mkHeadphone SUniqueMelody MestMKII) Poo Crinacle,
      Preset (mkHeadphone SUniqueMelody MestMKII) NoMedal Freeryder05,
      Preset (mkHeadphone SSennheiser HD599) Silver Crinacle,
      Preset (mkHeadphone SSennheiser HD599) Gold Oracle1990
    ]

easyEffectPresetMenu :: X ()
easyEffectPresetMenu =
  askListDoSimple
    [ ( ppPreset p,
        spawnProcess "easyeffects" ["-l", presetCliStr p]
      )
      | p <- presets
    ]
