module Audio where

import Base
import XMonad

data Sink = Sink
  { _sinkDescription :: String,
    _sinkId :: String
  }
  deriving stock (Eq, Show, Ord)

makeLenses ''Sink

volumeKeys :: [((KeyMask, KeySym), X ())]
volumeKeys =
  [ ((0, lowerVolumeKey), lowerVolume),
    ((0, raiseVolumeKey), raiseVolume),
    ((0, toggleSoundKey), toggleSound),
    ((shiftMask .|. modm, xK_plus), raiseVolume),
    ((shiftMask .|. modm, xK_minus), lowerVolume),
    ((shiftMask .|. modm, xK_0), toggleSound)
  ]

musicKey :: KeySym
musicKey = 0x1008ff81

lowerVolumeKey :: KeySym
lowerVolumeKey = 0x1008FF11

raiseVolumeKey :: KeySym
raiseVolumeKey = 0x1008FF13

toggleSoundKey :: KeySym
toggleSoundKey = 0x1008FF12

toggleSound :: X ()
toggleSound = spawn "pamixer --toggle-mute"

raiseVolume :: X ()
raiseVolume = do
  spawn "pamixer --increase 2"
  toastVol

lowerVolume :: X ()
lowerVolume = do
  spawn "pamixer --decrease 2"
  toastVol

toastVol :: X ()
toastVol = do
  vol <- liftIO getVolumeText
  say $ "Volume: " ++ vol

spawnMusicPlayer :: X ()
spawnMusicPlayer = spawn "clementine"

getVolume :: (MonadIO m) => m Int
getVolume = read <$> runProcessNoInput "pamixer" ["--get-volume"]

getVolumeText :: (MonadIO m) => m String
getVolumeText = runProcessNoInput "pamixer" ["--get-volume-human"]

getSinks :: (MonadIO m) => m [Sink]
getSinks = do
  ls :: [String] <-
    lines
      <$> runPipeNoInput
        [ ("pactl", ["list", "sinks"]),
          ("grep", ["-E", "Name:|Description:"]),
          ("cut", ["-f", "2-", "-d", " "])
        ]
  return (parseSinks (groupByPairs ls))
  where
    parseSinks :: [(String, String)] -> [Sink]
    parseSinks = \case
      [] -> []
      (idd, descr) : l ->
        Sink
          { _sinkId = idd,
            _sinkDescription = descr
          }
          : parseSinks l

    groupByPairs :: [a] -> [(a, a)]
    groupByPairs = \case
      [] -> []
      [_] -> error "the input list should have even length"
      a : b : l -> (a, b) : groupByPairs l

setDefaultSink :: (MonadIO m) => Sink -> m ()
setDefaultSink Sink {..} = spawnProcess "pactl" ["set-default-sink", _sinkId]

askSetDefaultSink :: X ()
askSetDefaultSink = do
  sinks <- sort <$> getSinks
  askListDo sinks (^. sinkDescription) setDefaultSink
