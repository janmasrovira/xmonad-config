Config
  { font = "xft:Bitstream Vera Sans Mono:size=14:bold:antialias=true",
    additionalFonts = [],
    borderColor = "black",
    border = TopB,
    bgColor = "black",
    fgColor = "grey",
    alpha = 255,
    position = Top,
    textOffset = -1,
    iconOffset = -1,
    lowerOnStart = True,
    pickBroadest = True,
    persistent = False,
    hideOnStart = False,
    iconRoot = ".",
    allDesktops = True,
    overrideRedirect = True,
    commands =
      [ Run
          Network
          "eth0"
          [ "-L",
            "0",
            "-H",
            "32",
            "--normal",
            "green",
            "--high",
            "red"
          ]
          10,
        Run
          Network
          "eth1"
          [ "-L",
            "0",
            "-H",
            "32",
            "--normal",
            "green",
            "--high",
            "red"
          ]
          10,
        Run
          Cpu
          [ "-L",
            "3",
            "-H",
            "50",
            "--normal",
            "green",
            "--high",
            "red"
          ]
          10,
        Run Memory ["-t", "Mem: <usedratio>%"] 10,
        -- NOTE: If Volume command is here xmobar does not start
        -- with xmonad, however, you can run it afterwards manually.
        -- I don't know why is that.
        -- , Run Volume "default" "Master" [] 2
        Run Swap [] 10,
        Run
          Battery
          [ "--template",
            "Battery(<acstatus>): <left>%",
            "--Low",
            "40",
            "--High",
            "60",
            "--high",
            "white",
            "--normal",
            "white",
            "--low",
            "orange"
          ]
          30,
        Run Date "%a %b %_d %Y %H:%M:%S" "date" 10
      ],
    sepChar = "%",
    alignSep = "}{",
    template =
      "%cpu% | %memory% * %swap% | %battery%}\
      \{ <fc=#ee9a00>%date%</fc>"
  }
