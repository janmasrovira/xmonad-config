#!/usr/bin/env bash

# in .xmonad
cd .xmonad
./build xmonad-x86_64-linux
cd ..

# create hard link or copy the file. Soft links won't work.
#sudo ln ~/.xmonad/xmonad.desktop /usr/share/xsessions/xmonad.desktop
sudo cp ~/.xmonad/xmonad.desktop /usr/share/xsessions/xmonad.desktop

# create xmonad soft link
mkdir -p ~/.local/bin
ln -si ~/.xmonad/xmonad-x86_64-linux ~/.local/bin/xmonad

# Sometimes this link is necessary
sudo ln -si ~/.local/bin/xmonad /usr/local/bin/xmonad

# stack must be on path
sudo ln -si ~/.ghcup/bin/stack /usr/local/bin/stack

# create xmonad badge for the login screen
# sudo convert xmonad.svg -resize 22x22 /usr/share/unity-greeter/custom_xmonad_badge.png
